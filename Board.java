public class Board{
	
	private Square[][] tictactoeBoard;
	
	public Board(){
		this.tictactoeBoard = new Square[3][3];
		for (int i = 0; i < this.tictactoeBoard.length; i++){
			for (int j = 0; j < this.tictactoeBoard[i].length; j++){
				this.tictactoeBoard[i][j] = Square.BLANK;
			}
		}
	}
	
	public String toString(){
		String statement = "";
		for (int i = 0; i < this.tictactoeBoard.length; i++){
			for (int j = 0; j < this.tictactoeBoard[i].length; j++){
				statement = statement + this.tictactoeBoard[i][j] + " ";
			}
			statement = statement + "\n";
		}
		return statement;
	}
	
	public boolean placeToken(int row, int col, Square playerToken){
		if (col < 1 && col > 3 && row < 1 && row > 3){
			return false;
		}
		else if (this.tictactoeBoard[row-1][col-1] == Square.BLANK){
			this.tictactoeBoard[row-1][col-1] = playerToken;
			return true;
		}
		else{
			return false;
		}
	}
	
	public boolean checkIfFull(){
		for (int i = 0; i < this.tictactoeBoard.length; i++){
			for (int j = 0; j < this.tictactoeBoard[i].length; j++){
				if (this.tictactoeBoard[i][j] == Square.BLANK){
					return false;
				}
			}
		}
		return true;
	}
	
	private boolean checkIfWinningHorizontal(Square playerToken){
		for (int i = 0; i < this.tictactoeBoard.length; i++){
			if (this.tictactoeBoard[i][0] == playerToken && this.tictactoeBoard[i][1] == playerToken && this.tictactoeBoard[i][2] == playerToken){
				return true;
			}
		}
		return false;
	}
	
	private boolean checkIfWinningVertical(Square playerToken){
		for (int j = 0; j < this.tictactoeBoard.length; j++){
			if (this.tictactoeBoard[0][j] == playerToken && this.tictactoeBoard[1][j] == playerToken && this.tictactoeBoard[2][j] == playerToken){
					return true;
			}
		}
		return false;
	}
	
	private boolean checkIfWinningDiagonal(Square playerToken){
		if (this.tictactoeBoard[0][0] == playerToken && this.tictactoeBoard[1][1] == playerToken && this.tictactoeBoard[2][2] == playerToken){
			return true;
		}
		else if (this.tictactoeBoard[0][2] == playerToken && this.tictactoeBoard[1][1] == playerToken && this.tictactoeBoard[2][0] == playerToken){
			return true;
		}
		else{
			return false;
		}
	}
	
	public boolean checkIfWinning(Square playerToken){
		if (checkIfWinningVertical(playerToken) || checkIfWinningHorizontal(playerToken) || checkIfWinningDiagonal(playerToken)){
			return true;
		}
		else{
			return false;
		}
	}
}