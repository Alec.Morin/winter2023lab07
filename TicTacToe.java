import java.util.Scanner;
public class TicTacToe{
	
	public static void main(String[]args){
		System.out.println("Welcome to Tic-Tac-Toe!");
		
		Board play = new Board();
		Scanner scan = new Scanner(System.in);
		
		boolean gameOver = false;
		int player = 1;
		Square playerToken = Square.X;
		
		while (!gameOver){
			System.out.println(play);
			
			if (player == 1){
				playerToken = Square.X;
			}
			else if (player == 0){
				playerToken = Square.O;
			}
			
			System.out.println("Which row and column would you like to select?");
			int row = scan.nextInt();
			int col = scan.nextInt();
			
			play.placeToken(row, col, playerToken);
			while (!(play.placeToken(row, col, playerToken))){
				System.out.println("Enter a new row and column value");
				row = scan.nextInt();
				col = scan.nextInt();
				play.placeToken(row, col, playerToken);
			}
			
			if (play.checkIfWinning(playerToken)){
				System.out.println("Player " + player + "is the winner!");
				gameOver = true;
			}
			else if (play.checkIfFull()){
				System.out.println("Its a tie!");
				gameOver = true;
			}
			else{
				player++;
				if (player > 2){
					player = 1;
				}
			}
		}
	}
}